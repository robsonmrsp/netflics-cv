/** generated: 09/04/2018 12:57:43 **/
package br.com.netflics.integration.controller;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.netflics.core.model.Owner;
import br.com.netflics.core.persistence.pagination.PaginationParams;
import br.com.netflics.core.security.SpringSecurityUserContext;

import br.com.netflics.json.JsonAvaliacao;
import br.com.netflics.model.Avaliacao;
import br.com.netflics.rs.AvaliacaoController;
import br.com.netflics.service.AvaliacaoService;

@RunWith(SpringRunner.class)
@WebMvcTest(AvaliacaoController.class)
public class AvaliacaoErrorMockTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private AvaliacaoService service;
	@MockBean
	private SpringSecurityUserContext context;

	@Test
	public void errorGetitingAvaliacaoById() throws Exception {
		when(service.get(any(Integer.class), any(Owner.class))).thenThrow(new RuntimeException("Error Getting Avaliacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/avaliacaos/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Avaliacao")));
	}

	@Test
	public void errorGetitingFilterEqualAvaliacao() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting Avaliacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/avaliacaos/filterEqual")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Avaliacao")));
	}

	@Test
	public void errorGetitingFilterAlikeAvaliacao() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting Avaliacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/avaliacaos/filterAlike")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Avaliacao")));
	}

	@Test
	public void errorGetitingAllAvaliacao() throws Exception {
		when(service.all(any(Owner.class))).thenThrow(new RuntimeException("Error Getting Avaliacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/avaliacaos/all")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Avaliacao")));
	}

	@Test
	public void errorGetitingAllPagerAvaliacao() throws Exception {
		when(service.all(any(PaginationParams.class),any(Owner.class))).thenThrow(new RuntimeException("Error Getting Avaliacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/avaliacaos")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Avaliacao")));
	}

	@Test
	public void errorPostitingAvaliacao() throws Exception {
		when(service.save(any(Avaliacao.class))).thenThrow(new RuntimeException("Error creating Avaliacao"));
		when(context.getOwner()).thenReturn(new Owner());
		this.mockMvc.perform(post("/rs/crud/avaliacaos").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error creating Avaliacao")));
	}
	
	@Test
	public void errorUpdatingAvaliacao() throws Exception {
		when(service.update(any(Avaliacao.class))).thenThrow(new RuntimeException("Error updating Avaliacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(put("/rs/crud/avaliacaos/1").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error updating Avaliacao")));
	}

	@Test
	public void errorDeletingAvaliacao() throws Exception {
		when(service.delete(any(Integer.class))).thenThrow(new RuntimeException("Error removing Avaliacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(delete("/rs/crud/avaliacaos/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error removing Avaliacao")));
	}

}