/** generated: 09/04/2018 12:57:44 **/
package br.com.netflics.integration.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.function.Consumer;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.netflics.core.persistence.pagination.Pager;
import br.com.netflics.model.Role;
import br.com.netflics.fixture.FixtureUtils;
import br.com.six2six.fixturefactory.Fixture;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Sql("classpath:init-data-Role.sql")
public class RoleControllerTest {

	@Autowired
	TestRestTemplate testRestTemplate;

	private static final String URL = "/rs/crud/roles";

	@BeforeClass
	public static void setUp() {
		FixtureUtils.init();
	}

	@Before
	public void before() {
	}

	@Test
	public void testAddRole() throws Exception {

		Role role = Fixture.from(Role.class).gimme("novo");
		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Role> responseEntity = withBasicAuth.postForEntity(URL, role, Role.class);

		HttpStatus status = responseEntity.getStatusCode();
		Role resultRole = responseEntity.getBody();

		assertEquals("Incorrect Response Status: ", HttpStatus.OK, status);
		assertNotNull("A not null gender should be returned: ", resultRole);
		assertNotNull("A not null gender identifier should be returned:", resultRole.getId());
	}

	@Test
	public void testGetRole() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Role> responseEntity = withBasicAuth.getForEntity(URL + "/{id}", Role.class, new Integer(1));

		HttpStatus status = responseEntity.getStatusCode();
		Role resultRole = responseEntity.getBody();

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);
		assertNotNull("A not null gender should be returned: ", resultRole);
		assertEquals("A id gender == 1 must be returned: ", resultRole.getId(), new Integer(1));
	}

	@Test
	public void testGetPagerRole() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Pager> responseEntity = withBasicAuth.getForEntity(URL, Pager.class);

		HttpStatus status = responseEntity.getStatusCode();
		Pager<Role> resultPagerRole = responseEntity.getBody();

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);
		assertNotNull("A not null gender should be returned: ", resultPagerRole);
	}

	@Test
	public void testGetRoleNotExist() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Role> responseEntity = withBasicAuth.getForEntity(URL + "/{id}", Role.class, new Long(100));

		HttpStatus status = responseEntity.getStatusCode();
		Role resultRole = responseEntity.getBody();

		assertEquals("Incorrect Response Status", HttpStatus.NO_CONTENT, status);
		assertNull(resultRole);
	}

	@Test
	public void testGetRoleFilterEqual() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Role[]> responseEntity = withBasicAuth.getForEntity(URL + "/filterEqual?authority={authority}", Role[].class,"authority role1");
		Role[] roles = responseEntity.getBody();
		HttpStatus status = responseEntity.getStatusCode();

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);

		assertTrue("A Array of Role should be returned ", roles.length > 0);

		Arrays.asList(roles).forEach(new Consumer<Role>() {
			@Override
			public void accept(Role role) {
				assertEquals("A not null Role should be returned white the 'name' = 'authority role1'", role.getAuthority(), "authority role1");
			}
		});
	}

	@Test
	public void testGetAllRole() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Role[]> responseEntity = withBasicAuth.getForEntity(URL + "/all", Role[].class);
		Role[] roles = responseEntity.getBody();
		HttpStatus status = responseEntity.getStatusCode();

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);

		assertTrue("A Array of Role should be returned ", roles.length > 0);

	}

	@Test
	public void testDeleteRole() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Boolean> responseEntity = withBasicAuth.exchange(URL + "/{id}", HttpMethod.DELETE, null, Boolean.class, new Integer(1));

		Boolean result = responseEntity.getBody();
		HttpStatus status = responseEntity.getStatusCode();

		ResponseEntity<Role> responseTesteDelete = withBasicAuth.getForEntity(URL + "/{id}", Role.class, new Integer(1));

		HttpStatus responseTesteDeleteStatus = responseTesteDelete.getStatusCode();
		Role resultRole = responseTesteDelete.getBody();

		assertEquals("Incorrect Response Status after delete the role id = 1", HttpStatus.NO_CONTENT, responseTesteDeleteStatus);
		assertNull(resultRole);

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);
		assertTrue("A Boolean.TRUE should be returned ", result);

	}

	@Test
	public void testGetRoleFilterALike() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(URL + "/filterAlike").queryParam("authority", "role");

		String uriString = builder.toUriString();
		ResponseEntity<Role[]> responseEntity = withBasicAuth.getForEntity(uriString, Role[].class);
		Role[] roles = responseEntity.getBody();
		HttpStatus status = responseEntity.getStatusCode();

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);

		assertTrue("A Array of Role should be returned ", roles.length > 0);

		Arrays.asList(roles).forEach(new Consumer<Role>() {
			@Override
			public void accept(Role role) {
				assertTrue("A not null Role should be returned white the 'name' like 'role'", role.getAuthority().contains("role"));
			}
		});
	}
}

// generated by JSetup v0.95 : at 18/10/2017 08:40:58
//generated by JSetup v0.95 :  at 09/04/2018 12:57:44