/** generated: 09/04/2018 12:57:43 **/
package br.com.netflics.integration.controller;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.netflics.core.model.Owner;
import br.com.netflics.core.persistence.pagination.PaginationParams;
import br.com.netflics.core.security.SpringSecurityUserContext;

import br.com.netflics.json.JsonGenero;
import br.com.netflics.model.Genero;
import br.com.netflics.rs.GeneroController;
import br.com.netflics.service.GeneroService;

@RunWith(SpringRunner.class)
@WebMvcTest(GeneroController.class)
public class GeneroErrorMockTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private GeneroService service;
	@MockBean
	private SpringSecurityUserContext context;

	@Test
	public void errorGetitingGeneroById() throws Exception {
		when(service.get(any(Integer.class), any(Owner.class))).thenThrow(new RuntimeException("Error Getting Genero"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/generos/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Genero")));
	}

	@Test
	public void errorGetitingFilterEqualGenero() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting Genero"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/generos/filterEqual")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Genero")));
	}

	@Test
	public void errorGetitingFilterAlikeGenero() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting Genero"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/generos/filterAlike")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Genero")));
	}

	@Test
	public void errorGetitingAllGenero() throws Exception {
		when(service.all(any(Owner.class))).thenThrow(new RuntimeException("Error Getting Genero"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/generos/all")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Genero")));
	}

	@Test
	public void errorGetitingAllPagerGenero() throws Exception {
		when(service.all(any(PaginationParams.class),any(Owner.class))).thenThrow(new RuntimeException("Error Getting Genero"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/generos")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Genero")));
	}

	@Test
	public void errorPostitingGenero() throws Exception {
		when(service.save(any(Genero.class))).thenThrow(new RuntimeException("Error creating Genero"));
		when(context.getOwner()).thenReturn(new Owner());
		this.mockMvc.perform(post("/rs/crud/generos").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error creating Genero")));
	}
	
	@Test
	public void errorUpdatingGenero() throws Exception {
		when(service.update(any(Genero.class))).thenThrow(new RuntimeException("Error updating Genero"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(put("/rs/crud/generos/1").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error updating Genero")));
	}

	@Test
	public void errorDeletingGenero() throws Exception {
		when(service.delete(any(Integer.class))).thenThrow(new RuntimeException("Error removing Genero"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(delete("/rs/crud/generos/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error removing Genero")));
	}

}