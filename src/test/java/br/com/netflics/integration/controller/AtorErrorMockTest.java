/** generated: 09/04/2018 12:57:44 **/
package br.com.netflics.integration.controller;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.netflics.core.model.Owner;
import br.com.netflics.core.persistence.pagination.PaginationParams;
import br.com.netflics.core.security.SpringSecurityUserContext;

import br.com.netflics.json.JsonAtor;
import br.com.netflics.model.Ator;
import br.com.netflics.rs.AtorController;
import br.com.netflics.service.AtorService;

@RunWith(SpringRunner.class)
@WebMvcTest(AtorController.class)
public class AtorErrorMockTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private AtorService service;
	@MockBean
	private SpringSecurityUserContext context;

	@Test
	public void errorGetitingAtorById() throws Exception {
		when(service.get(any(Integer.class), any(Owner.class))).thenThrow(new RuntimeException("Error Getting Ator"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/ators/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Ator")));
	}

	@Test
	public void errorGetitingFilterEqualAtor() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting Ator"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/ators/filterEqual")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Ator")));
	}

	@Test
	public void errorGetitingFilterAlikeAtor() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting Ator"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/ators/filterAlike")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Ator")));
	}

	@Test
	public void errorGetitingAllAtor() throws Exception {
		when(service.all(any(Owner.class))).thenThrow(new RuntimeException("Error Getting Ator"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/ators/all")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Ator")));
	}

	@Test
	public void errorGetitingAllPagerAtor() throws Exception {
		when(service.all(any(PaginationParams.class),any(Owner.class))).thenThrow(new RuntimeException("Error Getting Ator"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/ators")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Ator")));
	}

	@Test
	public void errorPostitingAtor() throws Exception {
		when(service.save(any(Ator.class))).thenThrow(new RuntimeException("Error creating Ator"));
		when(context.getOwner()).thenReturn(new Owner());
		this.mockMvc.perform(post("/rs/crud/ators").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error creating Ator")));
	}
	
	@Test
	public void errorUpdatingAtor() throws Exception {
		when(service.update(any(Ator.class))).thenThrow(new RuntimeException("Error updating Ator"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(put("/rs/crud/ators/1").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error updating Ator")));
	}

	@Test
	public void errorDeletingAtor() throws Exception {
		when(service.delete(any(Integer.class))).thenThrow(new RuntimeException("Error removing Ator"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(delete("/rs/crud/ators/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error removing Ator")));
	}

}