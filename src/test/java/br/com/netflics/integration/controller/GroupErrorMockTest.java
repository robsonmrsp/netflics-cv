/** generated: 09/04/2018 12:57:44 **/
package br.com.netflics.integration.controller;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.netflics.core.model.Owner;
import br.com.netflics.core.persistence.pagination.PaginationParams;
import br.com.netflics.core.security.SpringSecurityUserContext;

import br.com.netflics.json.JsonGroup;
import br.com.netflics.model.Group;
import br.com.netflics.rs.GroupController;
import br.com.netflics.service.GroupService;

@RunWith(SpringRunner.class)
@WebMvcTest(GroupController.class)
public class GroupErrorMockTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private GroupService service;
	@MockBean
	private SpringSecurityUserContext context;

	@Test
	public void errorGetitingGroupById() throws Exception {
		when(service.get(any(Integer.class), any(Owner.class))).thenThrow(new RuntimeException("Error Getting Group"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/groups/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Group")));
	}

	@Test
	public void errorGetitingFilterEqualGroup() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting Group"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/groups/filterEqual")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Group")));
	}

	@Test
	public void errorGetitingFilterAlikeGroup() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting Group"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/groups/filterAlike")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Group")));
	}

	@Test
	public void errorGetitingAllGroup() throws Exception {
		when(service.all(any(Owner.class))).thenThrow(new RuntimeException("Error Getting Group"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/groups/all")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Group")));
	}

	@Test
	public void errorGetitingAllPagerGroup() throws Exception {
		when(service.all(any(PaginationParams.class),any(Owner.class))).thenThrow(new RuntimeException("Error Getting Group"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/groups")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Group")));
	}

	@Test
	public void errorPostitingGroup() throws Exception {
		when(service.save(any(Group.class))).thenThrow(new RuntimeException("Error creating Group"));
		when(context.getOwner()).thenReturn(new Owner());
		this.mockMvc.perform(post("/rs/crud/groups").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error creating Group")));
	}
	
	@Test
	public void errorUpdatingGroup() throws Exception {
		when(service.update(any(Group.class))).thenThrow(new RuntimeException("Error updating Group"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(put("/rs/crud/groups/1").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error updating Group")));
	}

	@Test
	public void errorDeletingGroup() throws Exception {
		when(service.delete(any(Integer.class))).thenThrow(new RuntimeException("Error removing Group"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(delete("/rs/crud/groups/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error removing Group")));
	}

}