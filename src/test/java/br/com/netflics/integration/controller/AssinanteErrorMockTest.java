/** generated: 09/04/2018 12:57:43 **/
package br.com.netflics.integration.controller;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.netflics.core.model.Owner;
import br.com.netflics.core.persistence.pagination.PaginationParams;
import br.com.netflics.core.security.SpringSecurityUserContext;

import br.com.netflics.json.JsonAssinante;
import br.com.netflics.model.Assinante;
import br.com.netflics.rs.AssinanteController;
import br.com.netflics.service.AssinanteService;

@RunWith(SpringRunner.class)
@WebMvcTest(AssinanteController.class)
public class AssinanteErrorMockTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private AssinanteService service;
	@MockBean
	private SpringSecurityUserContext context;

	@Test
	public void errorGetitingAssinanteById() throws Exception {
		when(service.get(any(Integer.class), any(Owner.class))).thenThrow(new RuntimeException("Error Getting Assinante"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/assinantes/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Assinante")));
	}

	@Test
	public void errorGetitingFilterEqualAssinante() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting Assinante"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/assinantes/filterEqual")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Assinante")));
	}

	@Test
	public void errorGetitingFilterAlikeAssinante() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting Assinante"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/assinantes/filterAlike")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Assinante")));
	}

	@Test
	public void errorGetitingAllAssinante() throws Exception {
		when(service.all(any(Owner.class))).thenThrow(new RuntimeException("Error Getting Assinante"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/assinantes/all")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Assinante")));
	}

	@Test
	public void errorGetitingAllPagerAssinante() throws Exception {
		when(service.all(any(PaginationParams.class),any(Owner.class))).thenThrow(new RuntimeException("Error Getting Assinante"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/assinantes")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Assinante")));
	}

	@Test
	public void errorPostitingAssinante() throws Exception {
		when(service.save(any(Assinante.class))).thenThrow(new RuntimeException("Error creating Assinante"));
		when(context.getOwner()).thenReturn(new Owner());
		this.mockMvc.perform(post("/rs/crud/assinantes").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error creating Assinante")));
	}
	
	@Test
	public void errorUpdatingAssinante() throws Exception {
		when(service.update(any(Assinante.class))).thenThrow(new RuntimeException("Error updating Assinante"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(put("/rs/crud/assinantes/1").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error updating Assinante")));
	}

	@Test
	public void errorDeletingAssinante() throws Exception {
		when(service.delete(any(Integer.class))).thenThrow(new RuntimeException("Error removing Assinante"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(delete("/rs/crud/assinantes/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error removing Assinante")));
	}

}