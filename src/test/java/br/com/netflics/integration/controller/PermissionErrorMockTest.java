/** generated: 09/04/2018 12:57:44 **/
package br.com.netflics.integration.controller;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.netflics.core.model.Owner;
import br.com.netflics.core.persistence.pagination.PaginationParams;
import br.com.netflics.core.security.SpringSecurityUserContext;

import br.com.netflics.json.JsonPermission;
import br.com.netflics.model.Permission;
import br.com.netflics.rs.PermissionController;
import br.com.netflics.service.PermissionService;

@RunWith(SpringRunner.class)
@WebMvcTest(PermissionController.class)
public class PermissionErrorMockTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private PermissionService service;
	@MockBean
	private SpringSecurityUserContext context;

	@Test
	public void errorGetitingPermissionById() throws Exception {
		when(service.get(any(Integer.class), any(Owner.class))).thenThrow(new RuntimeException("Error Getting Permission"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/permissions/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Permission")));
	}

	@Test
	public void errorGetitingFilterEqualPermission() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting Permission"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/permissions/filterEqual")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Permission")));
	}

	@Test
	public void errorGetitingFilterAlikePermission() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting Permission"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/permissions/filterAlike")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Permission")));
	}

	@Test
	public void errorGetitingAllPermission() throws Exception {
		when(service.all(any(Owner.class))).thenThrow(new RuntimeException("Error Getting Permission"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/permissions/all")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Permission")));
	}

	@Test
	public void errorGetitingAllPagerPermission() throws Exception {
		when(service.all(any(PaginationParams.class),any(Owner.class))).thenThrow(new RuntimeException("Error Getting Permission"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/permissions")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Permission")));
	}

	@Test
	public void errorPostitingPermission() throws Exception {
		when(service.save(any(Permission.class))).thenThrow(new RuntimeException("Error creating Permission"));
		when(context.getOwner()).thenReturn(new Owner());
		this.mockMvc.perform(post("/rs/crud/permissions").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error creating Permission")));
	}
	
	@Test
	public void errorUpdatingPermission() throws Exception {
		when(service.update(any(Permission.class))).thenThrow(new RuntimeException("Error updating Permission"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(put("/rs/crud/permissions/1").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error updating Permission")));
	}

	@Test
	public void errorDeletingPermission() throws Exception {
		when(service.delete(any(Integer.class))).thenThrow(new RuntimeException("Error removing Permission"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(delete("/rs/crud/permissions/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error removing Permission")));
	}

}