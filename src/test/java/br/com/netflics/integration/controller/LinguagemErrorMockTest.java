/** generated: 09/04/2018 12:57:42 **/
package br.com.netflics.integration.controller;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.netflics.core.model.Owner;
import br.com.netflics.core.persistence.pagination.PaginationParams;
import br.com.netflics.core.security.SpringSecurityUserContext;

import br.com.netflics.json.JsonLinguagem;
import br.com.netflics.model.Linguagem;
import br.com.netflics.rs.LinguagemController;
import br.com.netflics.service.LinguagemService;

@RunWith(SpringRunner.class)
@WebMvcTest(LinguagemController.class)
public class LinguagemErrorMockTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private LinguagemService service;
	@MockBean
	private SpringSecurityUserContext context;

	@Test
	public void errorGetitingLinguagemById() throws Exception {
		when(service.get(any(Integer.class), any(Owner.class))).thenThrow(new RuntimeException("Error Getting Linguagem"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/linguagems/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Linguagem")));
	}

	@Test
	public void errorGetitingFilterEqualLinguagem() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting Linguagem"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/linguagems/filterEqual")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Linguagem")));
	}

	@Test
	public void errorGetitingFilterAlikeLinguagem() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting Linguagem"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/linguagems/filterAlike")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Linguagem")));
	}

	@Test
	public void errorGetitingAllLinguagem() throws Exception {
		when(service.all(any(Owner.class))).thenThrow(new RuntimeException("Error Getting Linguagem"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/linguagems/all")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Linguagem")));
	}

	@Test
	public void errorGetitingAllPagerLinguagem() throws Exception {
		when(service.all(any(PaginationParams.class),any(Owner.class))).thenThrow(new RuntimeException("Error Getting Linguagem"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/linguagems")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Linguagem")));
	}

	@Test
	public void errorPostitingLinguagem() throws Exception {
		when(service.save(any(Linguagem.class))).thenThrow(new RuntimeException("Error creating Linguagem"));
		when(context.getOwner()).thenReturn(new Owner());
		this.mockMvc.perform(post("/rs/crud/linguagems").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error creating Linguagem")));
	}
	
	@Test
	public void errorUpdatingLinguagem() throws Exception {
		when(service.update(any(Linguagem.class))).thenThrow(new RuntimeException("Error updating Linguagem"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(put("/rs/crud/linguagems/1").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error updating Linguagem")));
	}

	@Test
	public void errorDeletingLinguagem() throws Exception {
		when(service.delete(any(Integer.class))).thenThrow(new RuntimeException("Error removing Linguagem"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(delete("/rs/crud/linguagems/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error removing Linguagem")));
	}

}