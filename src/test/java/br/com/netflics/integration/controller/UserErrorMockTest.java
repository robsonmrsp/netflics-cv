/** generated: 09/04/2018 12:57:44 **/
package br.com.netflics.integration.controller;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.netflics.core.model.Owner;
import br.com.netflics.core.persistence.pagination.PaginationParams;
import br.com.netflics.core.security.SpringSecurityUserContext;

import br.com.netflics.json.JsonUser;
import br.com.netflics.model.User;
import br.com.netflics.rs.UserController;
import br.com.netflics.service.UserService;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserErrorMockTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserService service;
	@MockBean
	private SpringSecurityUserContext context;

	@Test
	public void errorGetitingUserById() throws Exception {
		when(service.get(any(Integer.class), any(Owner.class))).thenThrow(new RuntimeException("Error Getting User"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/users/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting User")));
	}

	@Test
	public void errorGetitingFilterEqualUser() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting User"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/users/filterEqual")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting User")));
	}

	@Test
	public void errorGetitingFilterAlikeUser() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting User"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/users/filterAlike")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting User")));
	}

	@Test
	public void errorGetitingAllUser() throws Exception {
		when(service.all(any(Owner.class))).thenThrow(new RuntimeException("Error Getting User"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/users/all")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting User")));
	}

	@Test
	public void errorGetitingAllPagerUser() throws Exception {
		when(service.all(any(PaginationParams.class),any(Owner.class))).thenThrow(new RuntimeException("Error Getting User"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/users")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting User")));
	}

	@Test
	public void errorPostitingUser() throws Exception {
		when(service.save(any(User.class))).thenThrow(new RuntimeException("Error creating User"));
		when(context.getOwner()).thenReturn(new Owner());
		this.mockMvc.perform(post("/rs/crud/users").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error creating User")));
	}
	
	@Test
	public void errorUpdatingUser() throws Exception {
		when(service.update(any(User.class))).thenThrow(new RuntimeException("Error updating User"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(put("/rs/crud/users/1").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error updating User")));
	}

	@Test
	public void errorDeletingUser() throws Exception {
		when(service.delete(any(Integer.class))).thenThrow(new RuntimeException("Error removing User"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(delete("/rs/crud/users/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error removing User")));
	}

}