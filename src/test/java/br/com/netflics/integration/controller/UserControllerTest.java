/** generated: 09/04/2018 12:57:44 **/
package br.com.netflics.integration.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.function.Consumer;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.netflics.core.persistence.pagination.Pager;
import br.com.netflics.model.User;
import br.com.netflics.fixture.FixtureUtils;
import br.com.six2six.fixturefactory.Fixture;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Sql("classpath:init-data-User.sql")
public class UserControllerTest {

	@Autowired
	TestRestTemplate testRestTemplate;

	private static final String URL = "/rs/crud/users";

	@BeforeClass
	public static void setUp() {
		FixtureUtils.init();
	}

	@Before
	public void before() {
	}

	@Test
	public void testAddUser() throws Exception {

		User user = Fixture.from(User.class).gimme("novo");
		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<User> responseEntity = withBasicAuth.postForEntity(URL, user, User.class);

		HttpStatus status = responseEntity.getStatusCode();
		User resultUser = responseEntity.getBody();

		assertEquals("Incorrect Response Status: ", HttpStatus.OK, status);
		assertNotNull("A not null gender should be returned: ", resultUser);
		assertNotNull("A not null gender identifier should be returned:", resultUser.getId());
	}

	@Test
	public void testGetUser() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<User> responseEntity = withBasicAuth.getForEntity(URL + "/{id}", User.class, new Integer(1));

		HttpStatus status = responseEntity.getStatusCode();
		User resultUser = responseEntity.getBody();

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);
		assertNotNull("A not null gender should be returned: ", resultUser);
		assertEquals("A id gender == 1 must be returned: ", resultUser.getId(), new Integer(1));
	}

	@Test
	public void testGetPagerUser() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Pager> responseEntity = withBasicAuth.getForEntity(URL, Pager.class);

		HttpStatus status = responseEntity.getStatusCode();
		Pager<User> resultPagerUser = responseEntity.getBody();

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);
		assertNotNull("A not null gender should be returned: ", resultPagerUser);
	}

	@Test
	public void testGetUserNotExist() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<User> responseEntity = withBasicAuth.getForEntity(URL + "/{id}", User.class, new Long(100));

		HttpStatus status = responseEntity.getStatusCode();
		User resultUser = responseEntity.getBody();

		assertEquals("Incorrect Response Status", HttpStatus.NO_CONTENT, status);
		assertNull(resultUser);
	}

	@Test
	public void testGetUserFilterEqual() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<User[]> responseEntity = withBasicAuth.getForEntity(URL + "/filterEqual?name={name}", User[].class,"name user1");
		User[] users = responseEntity.getBody();
		HttpStatus status = responseEntity.getStatusCode();

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);

		assertTrue("A Array of User should be returned ", users.length > 0);

		Arrays.asList(users).forEach(new Consumer<User>() {
			@Override
			public void accept(User user) {
				assertEquals("A not null User should be returned white the 'name' = 'name user1'", user.getName(), "name user1");
			}
		});
	}

	@Test
	public void testGetAllUser() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<User[]> responseEntity = withBasicAuth.getForEntity(URL + "/all", User[].class);
		User[] users = responseEntity.getBody();
		HttpStatus status = responseEntity.getStatusCode();

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);

		assertTrue("A Array of User should be returned ", users.length > 0);

	}

	@Test
	public void testDeleteUser() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		ResponseEntity<Boolean> responseEntity = withBasicAuth.exchange(URL + "/{id}", HttpMethod.DELETE, null, Boolean.class, new Integer(1));

		Boolean result = responseEntity.getBody();
		HttpStatus status = responseEntity.getStatusCode();

		ResponseEntity<User> responseTesteDelete = withBasicAuth.getForEntity(URL + "/{id}", User.class, new Integer(1));

		HttpStatus responseTesteDeleteStatus = responseTesteDelete.getStatusCode();
		User resultUser = responseTesteDelete.getBody();

		assertEquals("Incorrect Response Status after delete the user id = 1", HttpStatus.NO_CONTENT, responseTesteDeleteStatus);
		assertNull(resultUser);

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);
		assertTrue("A Boolean.TRUE should be returned ", result);

	}

	@Test
	public void testGetUserFilterALike() throws Exception {

		TestRestTemplate withBasicAuth = testRestTemplate.withBasicAuth("jsetup", "123456");

		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(URL + "/filterAlike").queryParam("name", "user");

		String uriString = builder.toUriString();
		ResponseEntity<User[]> responseEntity = withBasicAuth.getForEntity(uriString, User[].class);
		User[] users = responseEntity.getBody();
		HttpStatus status = responseEntity.getStatusCode();

		assertEquals("Incorrect Response Status", HttpStatus.OK, status);

		assertTrue("A Array of User should be returned ", users.length > 0);

		Arrays.asList(users).forEach(new Consumer<User>() {
			@Override
			public void accept(User user) {
				assertTrue("A not null User should be returned white the 'name' like 'user'", user.getName().contains("user"));
			}
		});
	}
}

// generated by JSetup v0.95 : at 18/10/2017 08:40:58
//generated by JSetup v0.95 :  at 09/04/2018 12:57:44