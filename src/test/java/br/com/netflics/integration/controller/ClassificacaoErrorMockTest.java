/** generated: 09/04/2018 12:57:44 **/
package br.com.netflics.integration.controller;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.netflics.core.model.Owner;
import br.com.netflics.core.persistence.pagination.PaginationParams;
import br.com.netflics.core.security.SpringSecurityUserContext;

import br.com.netflics.json.JsonClassificacao;
import br.com.netflics.model.Classificacao;
import br.com.netflics.rs.ClassificacaoController;
import br.com.netflics.service.ClassificacaoService;

@RunWith(SpringRunner.class)
@WebMvcTest(ClassificacaoController.class)
public class ClassificacaoErrorMockTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ClassificacaoService service;
	@MockBean
	private SpringSecurityUserContext context;

	@Test
	public void errorGetitingClassificacaoById() throws Exception {
		when(service.get(any(Integer.class), any(Owner.class))).thenThrow(new RuntimeException("Error Getting Classificacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/classificacaos/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Classificacao")));
	}

	@Test
	public void errorGetitingFilterEqualClassificacao() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting Classificacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/classificacaos/filterEqual")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Classificacao")));
	}

	@Test
	public void errorGetitingFilterAlikeClassificacao() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting Classificacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/classificacaos/filterAlike")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Classificacao")));
	}

	@Test
	public void errorGetitingAllClassificacao() throws Exception {
		when(service.all(any(Owner.class))).thenThrow(new RuntimeException("Error Getting Classificacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/classificacaos/all")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Classificacao")));
	}

	@Test
	public void errorGetitingAllPagerClassificacao() throws Exception {
		when(service.all(any(PaginationParams.class),any(Owner.class))).thenThrow(new RuntimeException("Error Getting Classificacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/classificacaos")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Classificacao")));
	}

	@Test
	public void errorPostitingClassificacao() throws Exception {
		when(service.save(any(Classificacao.class))).thenThrow(new RuntimeException("Error creating Classificacao"));
		when(context.getOwner()).thenReturn(new Owner());
		this.mockMvc.perform(post("/rs/crud/classificacaos").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error creating Classificacao")));
	}
	
	@Test
	public void errorUpdatingClassificacao() throws Exception {
		when(service.update(any(Classificacao.class))).thenThrow(new RuntimeException("Error updating Classificacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(put("/rs/crud/classificacaos/1").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error updating Classificacao")));
	}

	@Test
	public void errorDeletingClassificacao() throws Exception {
		when(service.delete(any(Integer.class))).thenThrow(new RuntimeException("Error removing Classificacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(delete("/rs/crud/classificacaos/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error removing Classificacao")));
	}

}