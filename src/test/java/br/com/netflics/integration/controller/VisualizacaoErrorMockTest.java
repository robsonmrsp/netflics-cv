/** generated: 09/04/2018 12:57:43 **/
package br.com.netflics.integration.controller;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.netflics.core.model.Owner;
import br.com.netflics.core.persistence.pagination.PaginationParams;
import br.com.netflics.core.security.SpringSecurityUserContext;

import br.com.netflics.json.JsonVisualizacao;
import br.com.netflics.model.Visualizacao;
import br.com.netflics.rs.VisualizacaoController;
import br.com.netflics.service.VisualizacaoService;

@RunWith(SpringRunner.class)
@WebMvcTest(VisualizacaoController.class)
public class VisualizacaoErrorMockTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private VisualizacaoService service;
	@MockBean
	private SpringSecurityUserContext context;

	@Test
	public void errorGetitingVisualizacaoById() throws Exception {
		when(service.get(any(Integer.class), any(Owner.class))).thenThrow(new RuntimeException("Error Getting Visualizacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/visualizacaos/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Visualizacao")));
	}

	@Test
	public void errorGetitingFilterEqualVisualizacao() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting Visualizacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/visualizacaos/filterEqual")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Visualizacao")));
	}

	@Test
	public void errorGetitingFilterAlikeVisualizacao() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting Visualizacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/visualizacaos/filterAlike")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Visualizacao")));
	}

	@Test
	public void errorGetitingAllVisualizacao() throws Exception {
		when(service.all(any(Owner.class))).thenThrow(new RuntimeException("Error Getting Visualizacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/visualizacaos/all")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Visualizacao")));
	}

	@Test
	public void errorGetitingAllPagerVisualizacao() throws Exception {
		when(service.all(any(PaginationParams.class),any(Owner.class))).thenThrow(new RuntimeException("Error Getting Visualizacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/visualizacaos")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Visualizacao")));
	}

	@Test
	public void errorPostitingVisualizacao() throws Exception {
		when(service.save(any(Visualizacao.class))).thenThrow(new RuntimeException("Error creating Visualizacao"));
		when(context.getOwner()).thenReturn(new Owner());
		this.mockMvc.perform(post("/rs/crud/visualizacaos").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error creating Visualizacao")));
	}
	
	@Test
	public void errorUpdatingVisualizacao() throws Exception {
		when(service.update(any(Visualizacao.class))).thenThrow(new RuntimeException("Error updating Visualizacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(put("/rs/crud/visualizacaos/1").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error updating Visualizacao")));
	}

	@Test
	public void errorDeletingVisualizacao() throws Exception {
		when(service.delete(any(Integer.class))).thenThrow(new RuntimeException("Error removing Visualizacao"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(delete("/rs/crud/visualizacaos/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error removing Visualizacao")));
	}

}