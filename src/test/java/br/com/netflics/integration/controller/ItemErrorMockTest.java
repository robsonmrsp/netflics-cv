/** generated: 09/04/2018 12:57:45 **/
package br.com.netflics.integration.controller;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.netflics.core.model.Owner;
import br.com.netflics.core.persistence.pagination.PaginationParams;
import br.com.netflics.core.security.SpringSecurityUserContext;

import br.com.netflics.json.JsonItem;
import br.com.netflics.model.Item;
import br.com.netflics.rs.ItemController;
import br.com.netflics.service.ItemService;

@RunWith(SpringRunner.class)
@WebMvcTest(ItemController.class)
public class ItemErrorMockTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ItemService service;
	@MockBean
	private SpringSecurityUserContext context;

	@Test
	public void errorGetitingItemById() throws Exception {
		when(service.get(any(Integer.class), any(Owner.class))).thenThrow(new RuntimeException("Error Getting Item"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/items/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Item")));
	}

	@Test
	public void errorGetitingFilterEqualItem() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting Item"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/items/filterEqual")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Item")));
	}

	@Test
	public void errorGetitingFilterAlikeItem() throws Exception {
		when(service.filter(any(PaginationParams.class), any(Owner.class), any(Boolean.class))).thenThrow(new RuntimeException("Error Getting Item"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/items/filterAlike")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Item")));
	}

	@Test
	public void errorGetitingAllItem() throws Exception {
		when(service.all(any(Owner.class))).thenThrow(new RuntimeException("Error Getting Item"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/items/all")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Item")));
	}

	@Test
	public void errorGetitingAllPagerItem() throws Exception {
		when(service.all(any(PaginationParams.class),any(Owner.class))).thenThrow(new RuntimeException("Error Getting Item"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(get("/rs/crud/items")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error Getting Item")));
	}

	@Test
	public void errorPostitingItem() throws Exception {
		when(service.save(any(Item.class))).thenThrow(new RuntimeException("Error creating Item"));
		when(context.getOwner()).thenReturn(new Owner());
		this.mockMvc.perform(post("/rs/crud/items").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error creating Item")));
	}
	
	@Test
	public void errorUpdatingItem() throws Exception {
		when(service.update(any(Item.class))).thenThrow(new RuntimeException("Error updating Item"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(put("/rs/crud/items/1").contentType(MediaType.APPLICATION_JSON).content("{}")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error updating Item")));
	}

	@Test
	public void errorDeletingItem() throws Exception {
		when(service.delete(any(Integer.class))).thenThrow(new RuntimeException("Error removing Item"));

		when(context.getOwner()).thenReturn(new Owner());

		this.mockMvc.perform(delete("/rs/crud/items/1")).andDo(print()).andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Error removing Item")));
	}

}