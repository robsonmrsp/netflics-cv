define(function(require) {
	var Router = require('Router');
	describe("Rotas", function() {

		beforeEach(function() {
			try {
				Backbone.history.stop();
			} catch (e) {
				console.error(e);
			}
		});
		
		afterEach(function() {
			// Reset URL
			var router = new Router();
			router.navigate("");
		});
		
				it("Rota de 'Linguagems'", function() {
			spyOn(Router.prototype, "linguagems")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/linguagems', true);
			expect(Router.prototype.linguagems).toHaveBeenCalled();
		});

		it("Rota de 'newLinguagem'", function() {
			spyOn(Router.prototype, "newLinguagem")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newLinguagem', true);
			expect(Router.prototype.newLinguagem).toHaveBeenCalled();
		});
		
		it("Rota de 'editLinguagem'", function() {
			spyOn(Router.prototype, "editLinguagem")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editLinguagem/1', true);
			expect(Router.prototype.editLinguagem).toHaveBeenCalled();
		});
		it("Rota de 'Avaliacaos'", function() {
			spyOn(Router.prototype, "avaliacaos")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/avaliacaos', true);
			expect(Router.prototype.avaliacaos).toHaveBeenCalled();
		});

		it("Rota de 'newAvaliacao'", function() {
			spyOn(Router.prototype, "newAvaliacao")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newAvaliacao', true);
			expect(Router.prototype.newAvaliacao).toHaveBeenCalled();
		});
		
		it("Rota de 'editAvaliacao'", function() {
			spyOn(Router.prototype, "editAvaliacao")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editAvaliacao/1', true);
			expect(Router.prototype.editAvaliacao).toHaveBeenCalled();
		});
		it("Rota de 'Assinantes'", function() {
			spyOn(Router.prototype, "assinantes")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/assinantes', true);
			expect(Router.prototype.assinantes).toHaveBeenCalled();
		});

		it("Rota de 'newAssinante'", function() {
			spyOn(Router.prototype, "newAssinante")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newAssinante', true);
			expect(Router.prototype.newAssinante).toHaveBeenCalled();
		});
		
		it("Rota de 'editAssinante'", function() {
			spyOn(Router.prototype, "editAssinante")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editAssinante/1', true);
			expect(Router.prototype.editAssinante).toHaveBeenCalled();
		});
		it("Rota de 'Generos'", function() {
			spyOn(Router.prototype, "generos")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/generos', true);
			expect(Router.prototype.generos).toHaveBeenCalled();
		});

		it("Rota de 'newGenero'", function() {
			spyOn(Router.prototype, "newGenero")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newGenero', true);
			expect(Router.prototype.newGenero).toHaveBeenCalled();
		});
		
		it("Rota de 'editGenero'", function() {
			spyOn(Router.prototype, "editGenero")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editGenero/1', true);
			expect(Router.prototype.editGenero).toHaveBeenCalled();
		});
		it("Rota de 'Criticos'", function() {
			spyOn(Router.prototype, "criticos")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/criticos', true);
			expect(Router.prototype.criticos).toHaveBeenCalled();
		});

		it("Rota de 'newCritico'", function() {
			spyOn(Router.prototype, "newCritico")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newCritico', true);
			expect(Router.prototype.newCritico).toHaveBeenCalled();
		});
		
		it("Rota de 'editCritico'", function() {
			spyOn(Router.prototype, "editCritico")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editCritico/1', true);
			expect(Router.prototype.editCritico).toHaveBeenCalled();
		});
		it("Rota de 'Filmes'", function() {
			spyOn(Router.prototype, "filmes")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/filmes', true);
			expect(Router.prototype.filmes).toHaveBeenCalled();
		});

		it("Rota de 'newFilme'", function() {
			spyOn(Router.prototype, "newFilme")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newFilme', true);
			expect(Router.prototype.newFilme).toHaveBeenCalled();
		});
		
		it("Rota de 'editFilme'", function() {
			spyOn(Router.prototype, "editFilme")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editFilme/1', true);
			expect(Router.prototype.editFilme).toHaveBeenCalled();
		});
		it("Rota de 'Visualizacaos'", function() {
			spyOn(Router.prototype, "visualizacaos")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/visualizacaos', true);
			expect(Router.prototype.visualizacaos).toHaveBeenCalled();
		});

		it("Rota de 'newVisualizacao'", function() {
			spyOn(Router.prototype, "newVisualizacao")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newVisualizacao', true);
			expect(Router.prototype.newVisualizacao).toHaveBeenCalled();
		});
		
		it("Rota de 'editVisualizacao'", function() {
			spyOn(Router.prototype, "editVisualizacao")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editVisualizacao/1', true);
			expect(Router.prototype.editVisualizacao).toHaveBeenCalled();
		});
		it("Rota de 'Classificacaos'", function() {
			spyOn(Router.prototype, "classificacaos")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/classificacaos', true);
			expect(Router.prototype.classificacaos).toHaveBeenCalled();
		});

		it("Rota de 'newClassificacao'", function() {
			spyOn(Router.prototype, "newClassificacao")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newClassificacao', true);
			expect(Router.prototype.newClassificacao).toHaveBeenCalled();
		});
		
		it("Rota de 'editClassificacao'", function() {
			spyOn(Router.prototype, "editClassificacao")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editClassificacao/1', true);
			expect(Router.prototype.editClassificacao).toHaveBeenCalled();
		});
		it("Rota de 'Ators'", function() {
			spyOn(Router.prototype, "ators")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/ators', true);
			expect(Router.prototype.ators).toHaveBeenCalled();
		});

		it("Rota de 'newAtor'", function() {
			spyOn(Router.prototype, "newAtor")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newAtor', true);
			expect(Router.prototype.newAtor).toHaveBeenCalled();
		});
		
		it("Rota de 'editAtor'", function() {
			spyOn(Router.prototype, "editAtor")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editAtor/1', true);
			expect(Router.prototype.editAtor).toHaveBeenCalled();
		});
		it("Rota de 'Users'", function() {
			spyOn(Router.prototype, "users")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/users', true);
			expect(Router.prototype.users).toHaveBeenCalled();
		});

		it("Rota de 'newUser'", function() {
			spyOn(Router.prototype, "newUser")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newUser', true);
			expect(Router.prototype.newUser).toHaveBeenCalled();
		});
		
		it("Rota de 'editUser'", function() {
			spyOn(Router.prototype, "editUser")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editUser/1', true);
			expect(Router.prototype.editUser).toHaveBeenCalled();
		});
		it("Rota de 'Roles'", function() {
			spyOn(Router.prototype, "roles")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/roles', true);
			expect(Router.prototype.roles).toHaveBeenCalled();
		});

		it("Rota de 'newRole'", function() {
			spyOn(Router.prototype, "newRole")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newRole', true);
			expect(Router.prototype.newRole).toHaveBeenCalled();
		});
		
		it("Rota de 'editRole'", function() {
			spyOn(Router.prototype, "editRole")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editRole/1', true);
			expect(Router.prototype.editRole).toHaveBeenCalled();
		});
		it("Rota de 'Permissions'", function() {
			spyOn(Router.prototype, "permissions")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/permissions', true);
			expect(Router.prototype.permissions).toHaveBeenCalled();
		});

		it("Rota de 'newPermission'", function() {
			spyOn(Router.prototype, "newPermission")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newPermission', true);
			expect(Router.prototype.newPermission).toHaveBeenCalled();
		});
		
		it("Rota de 'editPermission'", function() {
			spyOn(Router.prototype, "editPermission")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editPermission/1', true);
			expect(Router.prototype.editPermission).toHaveBeenCalled();
		});
		it("Rota de 'Groups'", function() {
			spyOn(Router.prototype, "groups")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/groups', true);
			expect(Router.prototype.groups).toHaveBeenCalled();
		});

		it("Rota de 'newGroup'", function() {
			spyOn(Router.prototype, "newGroup")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newGroup', true);
			expect(Router.prototype.newGroup).toHaveBeenCalled();
		});
		
		it("Rota de 'editGroup'", function() {
			spyOn(Router.prototype, "editGroup")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editGroup/1', true);
			expect(Router.prototype.editGroup).toHaveBeenCalled();
		});
		it("Rota de 'Items'", function() {
			spyOn(Router.prototype, "items")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/items', true);
			expect(Router.prototype.items).toHaveBeenCalled();
		});

		it("Rota de 'newItem'", function() {
			spyOn(Router.prototype, "newItem")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newItem', true);
			expect(Router.prototype.newItem).toHaveBeenCalled();
		});
		
		it("Rota de 'editItem'", function() {
			spyOn(Router.prototype, "editItem")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editItem/1', true);
			expect(Router.prototype.editItem).toHaveBeenCalled();
		});
	});
})
