/* generated by JSetup v0.95 :  at 09/04/2018 12:57:44 */
define(function(require) {
	var util = require('utilities/utils');
	var JSetup = require('views/components/JSetup');
	var Classificacao = require('models/Classificacao');
	
	var ModalClassificacaoTemplate = require('text!views/modalComponents/tpl/ModalClassificacaoTemplate.html');


	var ClassificacaoModal = JSetup.View.extend({
		template : _.template(ModalClassificacaoTemplate),

		/** The declared form Regions. */
		regions : {
			dataTableClassificacaoRegion : '.datatable-classificacao',
		},
		
		/** The form events you'd like to listen */
		events : {
			'click .btnSearchClassificacao' : 'searchClassificacao',
			'click .btnClearClassificacao' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		/** All the important fields must be here. */		
		ui : {
			loadButton : '.button-loading',
    		inputModalIdadeMinima : '.inputModalIdadeMinima',
    		inputModalNome : '.inputModalNome',
    		inputModalDescricao : '.inputModalDescricao',
		
			form : '#formSearchClassificacao',
			modalScreen : '.modal',
		},
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchClassificacao();
	    	}
		},

		/** First function called, like a constructor. */
		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;
			this.suggestConfig = opt.suggestConfig;

			this.classificacaoCollection = new Classificacao.PageCollection();
			this.classificacaoCollection.state.pageSize = 5;
			this.classificacaoCollection.on('fetching', this.startFetch, this);
			this.classificacaoCollection.on('fetched', this.stopFetch, this);
			
			this.dataTableClassificacao = new JSetup.DataTable({
				row : JSetup.RowClick,
				columns : this.getColumns(),
				collection : this.classificacaoCollection,
			});
			this.setValue(opt.initialValue);
		},
		
		/** Called after DOM´s ready.*/
		onShowView :  function() {
			var that = this;
			this.ui.inputModalIdadeMinima.integer();
		  	
		  	
		  	
	
			that.dataTableClassificacaoRegion.show(this.dataTableClassificacao);
					
			if (that.suggestConfig) {
				that.suggestConfig.collection = that.classificacaoCollection;
				that.suggestConfig.onSelect = function(json) {
					var model = new JSetup.BaseModel(json)
					that.onSelectModel(model);
					if (json) {
						that.modelSelect = model
					} else
						that.modelSelect = null;
				}
				util.configureSuggest(that.suggestConfig);
				that.suggestConfig.field.change(function(e) {
					if(!that.suggestConfig.field.val()){
						that.clear();
					}
				})
			}		
		},

		searchClassificacao : function() {
			var that = this;
			this.classificacaoCollection.filterQueryParams = {
		    	idadeMinima : this.ui.inputModalIdadeMinima.escape(true),
	    		nome : this.ui.inputModalNome.escape(), 
	    		descricao : this.ui.inputModalDescricao.escape(), 
			};

			this.classificacaoCollection.getFirstPage({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		selectRow : function(e) {
			var modelClassificacao = util.getWrappedModel(e);
			if (modelClassificacao){
				this.modelSelect = modelClassificacao; 
				this.onSelectModel(modelClassificacao);
			}
		},
		
		getJsonValue : function() {
			if (_.isEmpty(this.modelSelect) && _.isEmpty(this.jsonValue)) {
				return null;
			}
			if (this.modelSelect) {
				return this.modelSelect.toJSON();
			} else {
				return this.jsonValue;
			}
		},
		
		getRawValue : function() {
			var json = this.getJsonValue();
			if(json )
				return json.id
			return null;
		},
		
		getValue : function() {
			return this.modelSelect;
		},

		setValue : function(value) {
			this.jsonValue = value;
		},

		getColumns : function() {
			var columns = [	

		{
				name : "idadeMinima",
				sortable : true,
				editable : false,
				label 	 : "Idade Mínima",
				cell : JSetup.CustomStringCell
			}, 
					{
				name : "nome",
				sortable : true,
				editable : false,
				label 	 : "Nome",
				cell : JSetup.CustomStringCell
			}, 
					{
				name : "descricao",
				sortable : true,
				editable : false,
				label 	 : "Descrição",
				cell : JSetup.CustomStringCell
			}, 
						];
			return columns;
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();
			this.ui.modalScreen.modal('show');
			
			this.searchClassificacao();
		},

		clear : function() {
			this.clearModal();
		},
		clearModal : function() {
			this.modelSelect = null;
			this.jsonValue = null;
			this.classificacaoCollection.reset();
			util.scrollUpModal();
			this.ui.form.get(0).reset();
		},
		
		// Executada depois da consulta concluida.
		stopFetch : function() {
			this.ui.loadButton.button('reset');
			util.stopSpinner();
			util.scrollDownModal();
		},
		
		// Executada Antes da realização da consulta.
		startFetch : function() {
			this.ui.loadButton.button('loading');
			util.showSpinner('spinClassificacao');
		},
	});

	return ClassificacaoModal;
});
