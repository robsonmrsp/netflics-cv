/* generated by JSetup v0.95 :  at 09/04/2018 12:57:43 */
define(function(require) {
	var util = require('utilities/utils');
	var JSetup = require('views/components/JSetup');	
	var PageFilmeTemplate = require('text!views/filme/tpl/PageFilmeTemplate.html');

	var ModalClassificacao = require('views/modalComponents/ModalClassificacao');
	var Filme = require('models/Filme');
	var Linguagem = require('models/Linguagem');			

	var PageFilme = JSetup.View.extend({
		template : _.template(PageFilmeTemplate),

		/** The declared form Regions. */
		regions : {
			dataTableFilmeRegion : '.datatable-filme',
			modalClassificacaoRegion : '.modal-classificacao-container',
		},
		
		/** The form events you'd like to listen */
		events : {
			'click 	.reset-button' : 'resetFilme',			
			'click .search-classificacao-modal' : 'showModalClassificacao',
			'keypress' : 'treatKeypress',
			
			'click 	.search-button' : 'searchFilme',
			'click  .show-advanced-search-button' : 'toggleAdvancedForm',
		},
		
		/** All the important fields must be here. */		
		ui : {
			loadButton : '.loading-button',
			inputTituloOriginal : '#inputTituloOriginal',
			inputPoster : '#inputPoster',
			inputDuracao : '#inputDuracao',
			inputDataLancamento : '#inputDataLancamento',
			inputSinopse : '#inputSinopse',
			inputTitulo : '#inputTitulo',
			inputDiretor : '#inputDiretor',
			inputLinguagem : '#inputLinguagem', 
			inputClassificacaoId : '#inputClassificacaoId',
			inputClassificacaoNome : '#inputClassificacaoNome',
			form : '#formFilmeFilter',
			advancedSearchForm : '.advanced-search-form',
		},
		
		/** First function called, like a constructor. */		
		initialize : function() {
			var that = this;
			this.filmes = new Filme.PageCollection();
			
			this.dataTableFilme = new JSetup.DataTable({
				columns : this.getColumns(),
				collection : this.filmes,
				onFetching : this.startFetch,
				onFetched : this.stopFetch,
				view : this
			});			
			this.modalClassificacao = new ModalClassificacao({
				onSelectModel : function(model) {
					that.onSelectClassificacao(model);
				},
			});
		},

		/** Called after DOM´s ready.*/		
		onShowView : function() {
			var that = this;
			this.ui.inputDuracao.integer();
			this.ui.inputDataLancamento.date();
			this.comboLinguagem = new JSetup.Combobox({
				el : this.ui.inputLinguagem,
				comboId : 'id',
				comboVal : 'nome',
				collectionEntity : Linguagem.Collection,
			});
		
			this.dataTableFilmeRegion.show(this.dataTableFilme);
			this.modalClassificacaoRegion.show(this.modalClassificacao);		
		
			this.dataTableFilme.recoveryLastQuery();
		},
		
		 searchFilme : function(){		                                                                   
		 	var that = this;                                                                                                   
		 	this.dataTableFilme.getFirstPage({                                                                             
		 		success : function(_coll, _resp, _opt) {                                                                       
		 			//console.info('Consulta para o grid filme');                                         
		 		},                                                                                                             
		 		error : function(_coll, _resp, _opt) {                                                                         
		 			//console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));  
		 		},                                                                                                             
		 		filterQueryParams : {                                                                                          
		     		tituloOriginal : this.ui.inputTituloOriginal.escape(),                                   
		 	    	duracao : this.ui.inputDuracao.escape(true),                               
		     		dataLancamento : this.ui.inputDataLancamento.escape(),                                   
		     		sinopse : this.ui.inputSinopse.escape(),                                   
		     		titulo : this.ui.inputTitulo.escape(),                                   
		     		diretor : this.ui.inputDiretor.escape(),                                   
		 		    linguagem : this.comboLinguagem.getRawValue(),                                 
		 		    classificacao : this.modalClassificacao.getRawValue(),                                 
		 		}                                                                                                              
		 	})                                                                                                                 
		 },																													   
		
		resetFilme : function(){
			this.ui.form.get(0).reset();
			this.filmes.reset();
			 this.comboLinguagem.clear(); 
			 this.modalClassificacao.clear(); 
		},
				
		getColumns : function() {
			var that = this;
			var columns = [
			{
				name : "tituloOriginal",
				sortable : true,
				editable : false,
				label 	 : "Título Original",
				cell : JSetup.CustomStringCell
			}, 
			{
				name : "duracao",
				sortable : true,
				editable : false,
				label 	 : "Duração",
				cell : JSetup.CustomStringCell
			}, 
			{
				name : "dataLancamento",
				sortable : true,
				editable : false,
				label 	 : "Data do Lançamento",
				cell : JSetup.CustomStringCell
			}, 
			{
				name : "sinopse",
				sortable : true,
				editable : false,
				label 	 : "Sinopse",
				cell : JSetup.CustomStringCell
			}, 
			{
				name : "titulo",
				sortable : true,
				editable : false,
				label 	 : "Título",
				cell : JSetup.CustomStringCell
			}, 
			{
				name : "diretor",
				sortable : true,
				editable : false,
				label 	 : "Diretor",
				cell : JSetup.CustomStringCell
			}, 
			{
				name : "linguagem.nome",
				editable : false,
				sortable : true,  
				label : "Linguagem",
				cell : JSetup.CustomStringCell
			},	
			{
				name : "classificacao.nome",
				editable : false,  
				sortable : true,  
				label : "Classificação",
				cell : JSetup.CustomStringCell
			},	
			{
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				editable : false,
				sortable : false,
				cell : JSetup.ActionCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},
		
		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edit_button',
				type : 'primary',
				icon : 'fa-pencil',
				customClass : 'auth[edit-filme,disable]',
				hint : 'Editar Filme',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				customClass : 'auth[delete-filme, disable]',
				icon : 'fa-trash',
				hint : 'Remover Filme',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;
			
			var modelTipo = new Filme.Model({id : model.id});
			
			util.confirm({
				title : "Importante",
				text : "Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?",
				onConfirm : function() {
					modelTipo.destroy({
						success : function() {
							that.filmes.remove(model);
							util.alert({title : "Concluido", text : "Registro removido com sucesso!"});
						},
					});
				}
			});
		},

		editModel : function(model) {
			util.goPage("app/editFilme/" + model.get('id'));
		},

		showModalClassificacao : function() {
			this.modalClassificacao.showPage();
		},
			
		onSelectClassificacao : function(classificacao) {
			this.modalClassificacao.hidePage();	
			this.ui.inputClassificacaoId.val(classificacao.get('id'));
			this.ui.inputClassificacaoNome.val(classificacao.get('nome'));		
		},
		
		// additional functions
		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		treatKeypress : function(e) {
			if (util.enterPressed(e)) {
				e.preventDefault();
				this.searchFilme();
			}
		},
		startFetch : function() {
			util.loadButton(this.ui.loadButton)
		},

		stopFetch : function() {
			util.resetButton(this.ui.loadButton)
		}
	});

	return PageFilme;
});
