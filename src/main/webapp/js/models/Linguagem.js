/* generated by JSetup v0.95 :  at 09/04/2018 12:57:43 */
define(function(require) {
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var JSetup = require('views/components/JSetup');	

	var LinguagemModel = JSetup.BaseModel.extend({

		urlRoot : 'rs/crud/linguagems',

		defaults : {
			id: null,
	    	nome : '',    	
			filmes : null,
		
		}
	});
	
	var LinguagemCollection = JSetup.BaseCollection.extend({
		model : LinguagemModel,
		
		url : 'rs/crud/linguagems/all',
	});
	
	var LinguagemsCollection = JSetup.BasePageableCollection.extend({
		model : LinguagemModel,

		url : 'rs/crud/linguagems',

		mode : 'server',
	});

	var LinguagemPageClientCollection = LinguagemsCollection.extend({
		mode : 'client',
		state : {
			pageSize : 10,
		},
	})
	
	return {
		Model : LinguagemModel,
		Collection : LinguagemCollection,
	  	PageCollection : LinguagemsCollection,
	  	PageClientCollection : LinguagemPageClientCollection, 
	};
});
