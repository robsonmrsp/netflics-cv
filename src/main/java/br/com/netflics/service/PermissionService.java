package br.com.netflics.service;

import java.util.List;
import java.time.LocalDateTime;
import java.time.LocalDate;
import br.com.netflics.core.model.Owner;
import br.com.netflics.model.Permission;
import br.com.netflics.model.filter.FilterPermission;

import br.com.netflics.core.persistence.pagination.Pager;
import br.com.netflics.core.persistence.pagination.PaginationParams;
/* generated by JSetup v0.95 :  at 09/04/2018 12:57:44 */

public interface PermissionService {

	Permission get(Integer id);

	List<Permission> search(String searchText);
	
	Permission get(Integer id, Owner owner);

	List<Permission> all(Owner owner);
	
	List<Permission> filter(FilterPermission filterPermission, Owner owner, Boolean equals);
	
	List<Permission> filter(PaginationParams<FilterPermission> paginationParams, Owner owner, Boolean equals);
	
	List<Permission> filter(PaginationParams<FilterPermission> paginationParams, Owner owner);
	
	Pager<Permission> all(PaginationParams<FilterPermission> paginationParams, Owner owner);

	Permission save(Permission entity);

	Permission update(Permission entity);

	Boolean delete(Integer id);
			
}
//generated by JSetup v0.95 :  at 09/04/2018 12:57:44