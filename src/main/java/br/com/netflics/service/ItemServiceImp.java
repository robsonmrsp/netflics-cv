package br.com.netflics.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDateTime;
import java.time.LocalDate;
import br.com.netflics.core.model.Owner ;

import br.com.netflics.model.Item;
import br.com.netflics.persistence.DaoItem;
import br.com.netflics.model.filter.FilterItem;

import br.com.netflics.core.persistence.pagination.Pager;
import br.com.netflics.core.rs.exception.ValidationException;
import br.com.netflics.core.persistence.pagination.PaginationParams;
import br.com.netflics.core.utils.DateUtil;
import br.com.netflics.core.utils.Util;
/* generated by JSetup v0.95 :  at 09/04/2018 12:57:45 */

@Named
@Transactional
public class ItemServiceImp implements ItemService {

	private static final Logger LOGGER = Logger.getLogger(ItemServiceImp.class);
	
	@Inject
	DaoItem daoItem;

	@Override
	public Item get(Integer id) {
		return daoItem.find(id);
	}

	@Override
	public List<Item> search(String description) {
		return new ArrayList<Item>();
	}

	@Override
	public Item get(Integer id, Owner  owner) {
		return daoItem.find(id, owner);
	}

	@Override
	public List<Item> all(Owner  owner) {
		return daoItem.getAll(owner);
	}

	@Override
	public Pager<Item> all(PaginationParams<FilterItem> paginationParams, Owner  owner) {

		return daoItem.getAll(paginationParams, owner);
	}
	
	@Override
	public List<Item> filter(PaginationParams<FilterItem> paginationParams, Owner  owner) {
		List<Item> list = daoItem.filter(paginationParams, owner);
		return list;
	}
	
	@Override
	public List<Item> filter(PaginationParams<FilterItem> paginationParams,  Owner  owner, Boolean equals) {
		List<Item> list = daoItem.filter(paginationParams, owner, equals);
		return list;
	}
	
	@Override
	public List<Item> filter(FilterItem filterItem , Owner  owner,  Boolean equals) {
		List<Item> list = daoItem.filter(filterItem, owner, equals);
		return list;
	}

			
	@Override
	public Item save(Item entity) {
		return daoItem.save(entity);
	}

	@Override
	public Item update(Item entity) {
		return daoItem.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		Boolean del = Boolean.FALSE;
		try {
			del = daoItem.delete(id);
		} catch (ConstraintViolationException e) {
			throw new ValidationException(e, "Não é possível remover um registro que já está sendo utilizado por outro.");
		}
		return del;			
	}
}
//generated by JSetup v0.95 :  at 09/04/2018 12:57:45