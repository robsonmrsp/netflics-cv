package br.com.netflics.persistence;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Named;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.com.netflics.model.Linguagem;
import br.com.netflics.core.persistence.HibernateDao;
import br.com.netflics.core.persistence.pagination.PaginationParams;
import br.com.netflics.core.persistence.pagination.Pager;

import br.com.netflics.model.Linguagem;
import br.com.netflics.model.filter.FilterLinguagem;

import br.com.netflics.core.model.Owner;
/* generated by JSetup v0.95 :  at 09/04/2018 12:57:42 */
@Named
@SuppressWarnings("rawtypes")
public class DaoLinguagem extends HibernateDao<Linguagem> {
	private static final Logger LOGGER = Logger.getLogger(DaoLinguagem.class);

	public DaoLinguagem() {
		super(Linguagem.class);
	}
	public Linguagem findByNome(String nome, Owner owner) {

		Linguagem linguagem = null;
		try {
			CriteriaBuilder builder = builder();
			CriteriaQuery<Linguagem> query = query();
			Root<Linguagem> root = root();
		
			Predicate and = builder.and(builder.equal(root.get("nome"), nome), builder.equal(root.get("owner").get("id"), owner.getId()));

			linguagem = getSession().createQuery(query.select(root).where(and)).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("Erro ao obter Linguagem pelo nome," + nome, e);
		}
		return linguagem;
	}

//Consultas considerando o multitenancy	
	public Pager<Linguagem> getAll(PaginationParams<FilterLinguagem> queryParams, Owner owner) {
		FilterLinguagem filterLinguagem = queryParams.getFilter();
		CriteriaBuilder builder = builder();
		CriteriaQuery<Linguagem> query = query();
		Root<Linguagem> root = root();
		CriteriaQuery<Long> queryCount = queryCount();
		Root<Linguagem> rootCount = rootCount();

		
		Predicate whereClause = builder.and();
		
		whereClause = builder.and(whereClause, builder.equal(root.get("owner").get("id"),owner.getId()));
		
		if (filterLinguagem.getNome() != null) {
			whereClause = builder.and(whereClause, builder.like(builder.upper(root.<String>get("nome")), "%" + filterLinguagem.getNome().toUpperCase() + "%"));
		}
		Order orderBy = getOrderBy(queryParams);

		TypedQuery<Linguagem> typedQuery = getSession().createQuery(query.select(root).where(whereClause).orderBy(orderBy));
		TypedQuery<Long> typedCountQuery = getSession().createQuery(queryCount.select(builder.count(rootCount)).where(whereClause));

		List<Linguagem> results = typedQuery.setFirstResult(queryParams.getFirstResults()).setMaxResults(queryParams.getPageSize()).getResultList();
		Long total = typedCountQuery.getSingleResult();

		return new Pager<Linguagem>(results, queryParams.getPage(), total);
	}
	
	public List<Linguagem> filter(PaginationParams<FilterLinguagem> queryParams, Owner owner) {
		FilterLinguagem filterLinguagem = queryParams.getFilter();
		CriteriaBuilder builder = builder();
		CriteriaQuery<Linguagem> query = query();
		Root<Linguagem> root = root();
		
		Predicate whereClause = builder.and();

		whereClause = builder.and(whereClause, builder.equal(root.get("owner").get("id"),owner.getId()));
		
		if (filterLinguagem.getNome() != null) {
			whereClause = builder.and(whereClause, builder.like(builder.upper(root.<String>get("nome")), "%" + filterLinguagem.getNome().toUpperCase() + "%"));
		}
		Order orderBy = getOrderBy(queryParams);

		TypedQuery<Linguagem> typedQuery = getSession().createQuery(query.select(root).where(whereClause).orderBy(orderBy));
	
		List<Linguagem> results = typedQuery.setFirstResult(queryParams.getFirstResults()).setMaxResults(queryParams.getPageSize()).getResultList();
	
		return results;
	}

	public List<Linguagem> filter(PaginationParams paginationParams, Owner owner, Boolean equals) {
		List<Linguagem> list = new ArrayList<Linguagem>();
		FilterLinguagem filterLinguagem = (FilterLinguagem) paginationParams.getFilter();

		return filter(filterLinguagem, owner, equals);
	}



	public List<Linguagem> filter(FilterLinguagem filterLinguagem, Owner owner, Boolean equals) {
		if (equals) {
			return filterEqual(filterLinguagem, owner);
		} else {
			return filterAlike(filterLinguagem, owner);
		}
	}

	public List<Linguagem> filterEqual(FilterLinguagem filterLinguagem, Owner owner) {
		CriteriaBuilder builder = builder();
		CriteriaQuery<Linguagem> query = query();
		Root<Linguagem> root = root();
		
		Predicate whereClause = builder.and();

		whereClause = builder.and(whereClause, builder.equal(root.get("owner").get("id"), owner.getId()));

		if (filterLinguagem.getNome() != null) {
			whereClause = builder.and(whereClause, builder.equal(root.get("nome"), filterLinguagem.getNome()));
		}				
		TypedQuery<Linguagem> typedQuery = getSession().createQuery(query.select(root).where(whereClause));

		List<Linguagem> results = typedQuery.getResultList();

		return results;
	}

	public List<Linguagem> filterAlike(FilterLinguagem filterLinguagem, Owner owner) {
		CriteriaBuilder builder = builder();
		CriteriaQuery<Linguagem> query = query();
		Root<Linguagem> root = root();
		
		Predicate whereClause = builder.and();

		whereClause = builder.and(whereClause, builder.equal(root.get("owner").get("id"), owner.getId()));

		if (filterLinguagem.getNome() != null) {
			whereClause = builder.and(whereClause, builder.like(builder.upper(root.<String>get("nome")), "%" + filterLinguagem.getNome().toUpperCase() + "%"));
		}
	
		TypedQuery<Linguagem> typedQuery = getSession().createQuery(query.select(root).where(whereClause));

		List<Linguagem> results = typedQuery.getResultList();

		return results;
	}
	
}

//generated by JSetup v0.95 :  at 09/04/2018 12:57:42